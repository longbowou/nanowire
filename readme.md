# [Nanowire](https://nanowire.com)

Would you like to sell your unused bandwidth, or rent proxy connections?

## How to run it?

- Clone the repository
- Update **API_KEY** in **nanowire.toml**
- Run

```bash
docker-compose up -d
```